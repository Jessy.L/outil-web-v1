const darkMode = document.getElementById('dark-mode');
const etoileMode = document.getElementById('etoile-mode');
const backMode = document.getElementById('back-mode');

const root = document.documentElement.style;

var mode;

darkMode.addEventListener('click', changeMode);
darkMode.addEventListener('click', function() {SetCookie(mode);});


const btnDeleteCookie = document.getElementById('btn-cookie');
btnDeleteCookie.addEventListener('click', deleteCookie);


function changeMode(){

    if(mode == 0){
        
        etoileMode.src = "img/round-button-soleil.svg";
        backMode.src = "img/switch-elements-light.svg";

        etoileMode.style.left = "-15px";

        root.setProperty('--color-fond', "#FFFFFF" );
        root.setProperty('--color-bouton', "#127CFF" );
        root.setProperty('--color-bouton-hover', "#1875C4" );
        root.setProperty('--color-texte', "#FFFFFF" );
        root.setProperty('--color-texte-2', "#1D212C" );

        mode++;

    
    }else{

        etoileMode.src = "img/round-button-lune.svg";
        backMode.src = "img/switch-elements.svg";

        etoileMode.style.left = "12px";

        root.setProperty('--color-fond', "#1D212C" );
        root.setProperty('--color-bouton', "#2D82B7" );
        root.setProperty('--color-bouton-hover', "#155F8D" );
        root.setProperty('--color-texte', "#FFFFFF" );
        root.setProperty('--color-texte-2', "#FFFFFF" );

        mode = 0;


    }

    return mode;

}

function SetCookie(mode){

    var stock = mode;

    if(stock == 1){
        stock--;
    }else{
        stock++;
    }

    console.log(stock);

    let expireDays = 30;

    const date = new Date();
    
    date.setTime(date.getTime() + (expireDays*24*60*60*1000));

    let expires = "expires=" + date.toUTCString();

    document.cookie = "mode=" + stock + ";" + expires + ";path=/public";

    console.log('[SETCOOKIE] => ' + document.cookie);

}

function CheckCookie(){

    var dataCookie = [];
    
    let tabCookie = document.cookie.split(";");

    // console.table(tabCookie[0])

    index = 0

    do{


        var data = tabCookie[0].split('=');

        console.log('[CHECKCOOKIE] => ' + document.cookie);

        dataCookie.push(data);

        index = index + 1;

    }while(index <= tabCookie.length);

    
    // console.log(dataCookie)


    if(dataCookie[0][0] != 'mode' || parseInt(dataCookie[0][1]) == NaN ){
        mode = 0;
    }else{
        mode = dataCookie[0][1];
        console.log(mode);

    }

    changeMode(mode);

}

function deleteCookie(){
    document.cookie = "mode=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/public;";
    alert('Delete cookie : OK ');
}

CheckCookie();



